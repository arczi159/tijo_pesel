package pl.edu.pwsztar;

import java.util.Arrays;
import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL
    private final int[] WEIGHTS = new int[]{1,3,7,9,1,3,7,9,1,3};

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        if(id == null ||id.equals("")){
            return false;
        }

        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        if(!isCorrect()){
            return Optional.empty();
        }
        final int sexInteger = Integer.parseInt(String.valueOf(id.charAt(9)));
        if(sexInteger%2 == 0){
            return Optional.of(Sex.WOMAN);
        }else{
            return Optional.of(Sex.MAN);
        }
    }

    @Override
    public boolean isCorrect() {
        final int[] id = Arrays.asList(this.id.split("")).stream().mapToInt(Integer::parseInt).toArray();
        int sum = 0;
        for (int i = 0; i < id.length-1; i++) {
            sum += WEIGHTS[i]*id[i];
        }

        return 10-(sum%10)==id[10];
    }

    @Override
    public Optional<String> getDate() {
        if(!isCorrect()){
            return Optional.empty();
        }

        final String day = id.substring(4,6);
        String month = id.substring(2,4);
        final String year = getYear(month);

        month = getCorrectMonth(id.substring(2,4),year);
        return Optional.of(day + "-" + month + "-" + year);
    }

    private String getYear(String month) {
        String year;
        if(month.charAt(0)=='0'||month.charAt(0)=='1'){
            year = "19"+id.substring(0,2);
        }else{
            year = "20"+id.substring(0,2);
        }
        return year;
    }

    private String getCorrectMonth(String month, String year){
        if(month.charAt(0)=='0'){
            return month;
        }else{
            if(Integer.parseInt(year)>=2000){
                return "0"+ (Integer.parseInt(month) - 20);
            }
        }
        return month;
    }

}
