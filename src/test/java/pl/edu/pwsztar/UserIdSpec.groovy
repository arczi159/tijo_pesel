package pl.edu.pwsztar

import spock.lang.Specification

class UserIdSpec extends Specification {

    def "should check correct size"() {
        given:
            UserId userId = new UserId("12345678901")
        when:
            def isCorrectSize = userId.isCorrectSize()
        then:
            isCorrectSize
    }

    def "should check incorrect size"() {
        given:
            UserId userId = new UserId("12")
        when:
            def isCorrectSize = userId.isCorrectSize()
        then:
            !isCorrectSize
    }

    def "should check null"() {
        given:
            UserId userId = new UserId(null)
        when:
            def isCorrectSize = userId.isCorrectSize()
        then:
            !isCorrectSize
        //
    }

    def "should check empty value"() {
        given:
            UserId userId = new UserId("")
        when:
            boolean isCorrectSize = userId.isCorrectSize()
        then:
            !isCorrectSize
    }

    def "should check user sex"(){
        given:
            UserId man = new UserId("00231734857")
            UserId woman = new UserId("95070685769")
            UserId none = new UserId("21512948924")
        when:
            Optional<UserIdChecker.Sex> sex1 = man.getSex();
            Optional<UserIdChecker.Sex> sex2 = woman.getSex();
            Optional<String> sex3 = none.getSex();
        then:
            sex1.get() == UserIdChecker.Sex.MAN;
            sex2.get() == UserIdChecker.Sex.WOMAN;
            sex3 == Optional.empty();
    }

    def "should check if pesel is valid"() {
        given:
            UserId user1 = new UserId("12231734857");
            UserId user2 = new UserId("98071310847");
        when:
            boolean isNotValid = user1.isCorrect();
            boolean isValid = user2.isCorrect();
        then:
            !isNotValid
            isValid
    }

    def "should return date from id" (){
        given:
            UserId userId1 = new UserId("01291425338");
            UserId userId2 = new UserId("70080733958");
            UserId userId3 = new UserId("51112948984");
            UserId userId4 = new UserId("21112948984");
        when:
            String date1 = userId1.getDate().get()
            String date2 = userId2.getDate().get()
            String date3 = userId3.getDate().get()
            Optional<String> date4 = userId4.getDate()
        then:
            date1 == "14-09-2001"
            date2 == "07-08-1970"
            date3 == "29-11-1951"
            date4 == Optional.empty()
    }
}
